// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'project';

module.exports = {
  Project: {
    roles: async (obj, args, ctx, info) => {
      if (obj.roles_ids) {
        return await ctx.db.competence.find({ _id: { $in: obj.roles_ids } });
      }
    },
  },
  Precedent: {
    project: async (obj, args, ctx, info) => {
      if (obj.project_id) {
        return (await ctx.db.project.find({ _id: new ObjectId(obj.project_id) }))[0];
      }
    },
  },
};
